-- Создание индекса full text search
CREATE INDEX full_text_search_table_big_text_fts_idx 
ON full_text_search_table 
USING GIN (to_tsvector('russian', big_text));

-- Удаление индекса
DROP INDEX full_text_search_table_big_text_fts_idx;

-- Полнотекстовый поиск
-- Без индекса - 280 ms
-- С индексом - 55 ms
SELECT big_text, 
    ts_rank(
        to_tsvector('russian', big_text), 
        plainto_tsquery('russian', 'спрятал')
    ) AS rank
FROM full_text_search_table
WHERE to_tsvector('russian', big_text) 
    @@ plainto_tsquery('russian', 'спрятал')
ORDER BY rank DESC;
