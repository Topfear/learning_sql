-- Транзакция блокирующая строку на 6 секунд
-- и обновляющая в ней значение
-- Упадёт, если строка 5000 
-- заблокирована другой транзакцией
BEGIN;
SELECT * 
FROM test_table 
WHERE id = 5000
FOR UPDATE NOWAIT;

SELECT pg_sleep(6);

UPDATE test_table 
SET test_text = 'New text. The record id: ' || id::text
WHERE id = 5000
RETURNING *;
END;

-- Транзакция блокирующая строку на 6 секунд
-- и обновляющая в ней значение
-- Упадёт, если строка 5000 
-- заблокирована другой транзакцией
BEGIN;
SELECT * 
FROM test_table 
WHERE id = 5000
FOR UPDATE NOWAIT;

SELECT pg_sleep(6);

UPDATE test_table 
SET test_text = 'Conflict detected'
WHERE id = 5000
RETURNING *;
END;

-- Проверить результат
SELECT * 
FROM test_table 
WHERE id = 5000;
