-- Вставка строк в таблицу
INSERT INTO test_table (test_text) 
VALUES
    ('Hello world!'),
    ('The SQL game');

-- Вставка строк с возвращением id'шников
INSERT INTO test_table (id, test_text) 
VALUES
    (DEFAULT, 'New row'),
    (DEFAULT, 'Brand new row')
RETURNING id;

-- Вставка или обновление строки.
-- Эквивалент update_or_create
INSERT INTO test_table (id, test_text) 
VALUES 
    (1, 'First row'), 
    (2, 'Second row')
ON CONFLICT (id) 
    DO UPDATE SET test_text = EXCLUDED.test_text
RETURNING *;

-- Вставка без возвращения ошибок
INSERT INTO test_table (id, test_text) 
VALUES 
    (1, 'Redline Miami')
ON CONFLICT (id) DO NOTHING;
