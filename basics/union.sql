-- Соберет уникальные значения из обеих таблиц
SELECT *
FROM test_table
WHERE id BETWEEN 10 AND 15
UNION
SELECT *
FROM test_table
WHERE id BETWEEN 13 AND 20
ORDER BY id;

-- Соберет неуникальные (с повторениями) значения
-- из обеих таблиц
SELECT *
FROM test_table
WHERE id BETWEEN 10 AND 15
UNION ALL
SELECT *
FROM test_table
WHERE id BETWEEN 13 AND 20
ORDER BY id;
