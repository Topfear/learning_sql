-- Обновить строки 
UPDATE test_table 
SET test_text = 'The record id: ' || id::text
WHERE id >= 10 AND id <= 50;

-- Обновить строки и вернуть их
UPDATE test_table 
SET test_text = 'New text. The record id: ' || id::text
WHERE id >= 10 AND id <= 50
RETURNING *;
