-- Ленивый способ заглянуть в таблицу.
-- Из недостатков - нельзя использовать WHERE и JOIN 
TABLE test_table
LIMIT 1000;

-- Предыдущий запрос эквивалентен этому
SELECT *
FROM test_table
LIMIT 1000;

-- Выборка в диапазоне
SELECT *
FROM test_table
WHERE id BETWEEN 1 AND 5000;
