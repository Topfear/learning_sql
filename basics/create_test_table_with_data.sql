-- Создать/пересоздать тестовую таблицу
DROP TABLE IF EXISTS test_table;
CREATE TABLE test_table (
	id serial PRIMARY KEY,
	test_text VARCHAR(300) NOT NULL,
	test_number INTEGER NOT NULL,
	test_boolean BOOLEAN NOT NULL
);

-- Добавить столбец в таблицу
ALTER TABLE test_table
    ADD test_number_2 INTEGER NOT NULL DEFAULT 0;

-- Вставить 50_000 записей, ~ 0.5 секунд на выполнение ☕
INSERT INTO test_table (test_text, test_number, test_boolean)
SELECT
    md5(number::text),
	ceil(random() * 10), -- 1 - 10
	random() > 0.5 -- 50% true, 50% false
FROM generate_series(1, 50000) AS number;

-- Проверить размер таблицы вместе с индексами
SELECT pg_size_pretty(pg_total_relation_size('test_table')); -- 4880 kB
-- Проверить размер таблицы без индексов
SELECT pg_size_pretty(pg_table_size('test_table')); -- 3776 kB
-- Проверить размер индексов в таблице
SELECT pg_size_pretty(pg_indexes_size('test_table')); -- 1112 kB

-- Удалить тестовую таблицу
DROP TABLE test_table;
