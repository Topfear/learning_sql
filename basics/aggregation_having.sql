-- min|max по таблице
SELECT min(test_number), max(test_number)
FROM test_table;

-- Посчитать кол-во единиц в таблице
SELECT COUNT(*)
FROM test_table
WHERE test_number = 1;

-- Посчитать кол-во цифр, их сумму 
-- и среднее значение по группам
-- а также all - все значения правдивы 
-- или any - хотябы одно правдиво
SELECT test_number, 
    COUNT(test_number), SUM(test_number), AVG(test_number),
    BOOL_OR(test_boolean) AS bool_any, 
    BOOL_AND(test_boolean) AS bool_all
FROM test_table
GROUP BY test_number
ORDER BY test_number;

-- Посчитать кол-во цифр по группам, где кол-во чисел больше 5000
SELECT test_number, COUNT(test_number)
FROM test_table
GROUP BY test_number
HAVING COUNT(test_number) >= 5000
ORDER BY test_number;
