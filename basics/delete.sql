-- Удалить все строки из таблицы
DELETE FROM test_table;

-- Удалить строки в диапазоне и вернуть из
DELETE FROM test_table
WHERE id BETWEEN 1 AND 1000000 
RETURNING *;

-- Удалить таблицу совем
DROP TABLE test_table;
