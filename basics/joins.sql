-- Создать/пересоздать таблицу с авторами
DROP TABLE IF EXISTS authors;
CREATE TABLE authors (
	id serial PRIMARY KEY,
	name VARCHAR(30) NOT NULL
);

-- Создать/пересоздать таблицу с книгами
DROP TABLE IF EXISTS books;
CREATE TABLE books (
	id serial PRIMARY KEY,
	name VARCHAR(30) NOT NULL,
    autor_id INTEGER REFERENCES authors(id)
);

-- Вставить записи о авторах
INSERT INTO authors (name)
VALUES
    ('Михаил Булгаков'),
    ('Николай Гоголь'),
    ('Федор Достоевский'),
	('Автор без книг')
RETURNING *;

-- Вставить записи о книгах
INSERT INTO books (name, autor_id)
VALUES
    ('Мастер и Маргарита', 1),
    ('Собачье сердце', 1),
    ('Мёртвые души', 2),
	('Ревизор', 2),
	('Преступление и наказание', 3),
	('Книга без автора', null)
RETURNING *;

-- Получить авторов и их книги 
-- только если связь есть (INNER JOIN)
SELECT 
	authors.id AS author_id, authors.name, 
	books.id AS book_id, books.name
FROM authors
JOIN books ON authors.id = books.autor_id;

-- Получить всех авторов с книгами или без,
-- книги будут в результате запроса если они есть (LEFT JOIN)
SELECT 
	authors.id AS author_id, authors.name, 
	books.id AS book_id, books.name
FROM authors
LEFT JOIN books ON authors.id = books.autor_id;

-- Получить всех авторов и все книги,
-- вне зависимости есть связь с другой таблицей или нет 
-- (FULL OUTER JOIN)
SELECT 
	authors.id AS author_id, authors.name, 
	books.id AS book_id, books.name
FROM authors
FULL OUTER JOIN books ON authors.id = books.autor_id;
