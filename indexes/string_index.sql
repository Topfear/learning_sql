-- Создание индекса на text колонку
-- занимает примерно 1 min 39sec ☕
-- Для varchar нужно создавать 
-- специальный индекс varchar_pattern_ops.
-- Такой индекс работает только с
-- конструкциями типа LIKE 'search_string%',
-- и не работает с LIKE '%search_string%',
CREATE INDEX test_index_table_test_text_idx 
ON test_index_table (test_text varchar_pattern_ops);

-- Удаление индекса
DROP INDEX test_index_table_test_text_idx;


-- Выборка по string колонке
-- Без индекса - 38 sec
-- С индексом - 63ms
SELECT *
FROM test_index_table
WHERE test_text LIKE 'b32c%'
LIMIT 1000;

-- Проверить размер индекса
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_test_text_idx'));


-- Если мы хотим искать по регулярным выражениям 
-- или искать паттерны по середине текста, 
-- нам понадобится расширение pg_trgm
CREATE EXTENSION pg_trgm;

-- Создаём триграм индекс 
-- создаём 12 min 6sec
CREATE INDEX test_index_table_test_text_trgm_idx 
ON test_index_table USING GIN (test_text gin_trgm_ops);


-- Выборка по string колонке
-- Без индекса - 37 sec
-- С индексом - 150 мс
SELECT *
FROM test_index_table
WHERE test_text LIKE '%b32c7b%'
LIMIT 1000;

-- Проверить размер индекса
-- ооочень ресурсоёмкий индекс
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_test_text_trgm_idx'));


-- Лучше не использовать эту функцию
-- на больших данных
SELECT 
    test_text, 
    similarity(
        test_text, 
        '4b1dcdf14c9244'
    ) AS sml
FROM test_index_table
WHERE test_text % '4b1dcdf14c9244'
LIMIT 10;
