-- Создание индекса на date колонку
-- занимает примерно 59 sec ☕
CREATE INDEX test_index_table_test_date_idx 
ON test_index_table (test_date);

-- Удаление индекса
DROP INDEX test_index_table_test_date_idx;

-- Выборка по date колонке
-- Без индекса - 9.5 sec
-- С индексом - 200 ms
SELECT *
FROM test_index_table
WHERE test_index_table.test_date = '2022-06-16'
LIMIT 1000;

-- Проверить размер индекса
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_test_date_idx'));
