-- Создать/пересоздать тестовую таблицу
DROP TABLE IF EXISTS test_index_table;
CREATE TABLE test_index_table (
	id serial PRIMARY KEY,
	test_text VARCHAR(300) NOT NULL,
	test_number INTEGER NOT NULL,
	test_float REAL NOT NULL,
	test_boolean BOOLEAN NOT NULL,
	test_uuid UUID NOT NULL,
	test_date DATE NOT NULL
);

-- Вставить 50_000_000 записей, ~6 min 38sec на выполнение ☕
INSERT INTO test_index_table (
	test_text, test_number, test_float,
	test_boolean, test_uuid, test_date)
SELECT
    md5(number::text),
	ceil(random() * 10), 	-- int в диапазоне 1 - 10
	random() * 9 + 1, 		-- float в диапазоне 1 - 10
	random() > 0.5, 		-- bool где 50% true, 50% false
	gen_random_uuid(),		-- случайный uuid
	'2022-06-20'::date-((random() * 10000)::INTEGER + 1) -- дата в диапазоне
FROM generate_series(1, 50000000) AS number;

-- Проверить 1000 первых записей
TABLE test_index_table
LIMIT 1000;

-- Проверить кол-во записей в таблице
SELECT COUNT(*)
FROM test_index_table;

-- Удалить тестовую таблицу
DROP TABLE test_index_table;
