-- Проверить размер таблицы без индексов
SELECT pg_size_pretty(pg_table_size('test_index_table'));

-- Отобразить все индексы в таблице
SELECT * FROM pg_indexes WHERE tablename = 'test_index_table';

-- Проверить размер всех индексов в таблице
SELECT pg_size_pretty(pg_indexes_size('test_index_table'));

-- Проверить размер индекса test_index_table_pkey
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_pkey'));

-- Проверить размер индекса test_index_table_test_number_idx
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_test_number_idx'));

-- Проверить размер индекса test_index_table_test_float_idx
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_test_float_idx'));

-- Проверить размер индекса test_index_table_test_date_idx
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_test_date_idx'));
