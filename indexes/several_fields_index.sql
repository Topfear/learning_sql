-- Создание индекса на int колонку
-- занимает примерно 1 min 35sec ☕
CREATE INDEX test_index_table_test_number_test_float_test_date_idx 
ON test_index_table (test_number, test_float, test_date);

-- Удаление индекса
DROP INDEX test_index_table_test_number_test_float_test_date_idx;

-- Выборка по нескольким столбцам
-- Без индекса - 700 ms
-- С индексом - 180 ms
SELECT *
FROM test_index_table
WHERE test_index_table.test_float BETWEEN 4.0 AND 4.1
    AND test_index_table.test_number = 10
    AND test_index_table.test_date = '2022-06-18'
LIMIT 1000;

-- Проверить размер индекса
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_test_number_test_float_test_date_idx'));
