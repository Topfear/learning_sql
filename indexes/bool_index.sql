-- WARNING! WARNING! WARNING!
-- Нет смысла создавать индекс на boolean столбец,
-- из-за маленькой селективности индекса.
-- Имеет смысл создать HASH индекс,
-- если, например, записей со значением true - 10 штук.
-- WARNING! WARNING! WARNING!

-- Создание индекса на boolean колонку
-- занимает примерно 59 sec ☕
CREATE INDEX test_index_table_test_boolean_idx 
ON test_index_table (test_boolean);

-- Удаление индекса
DROP INDEX test_index_table_test_boolean_idx;

-- Выборка по boolean колонке
-- Без индекса - 200 мс
-- С индексом - 200 мс
SELECT *
FROM test_index_table
WHERE test_index_table.test_boolean = true
LIMIT 1000;

-- Проверить размер индекса
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_test_boolean_idx'));
