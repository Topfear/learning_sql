-- Создание индекса на uuid колонку
-- занимает примерно 2 min 20sec ☕
-- Обычно при поиске uuid используется 
-- только оператор =, поэтому был выбран
-- индекс типа HASH.

-- WARNING! WARNING! WARNING!
-- HASH индекс поддерживает только оператор
-- сравнивания (т.е. =), другие операторы не поддерживает.
-- WARNING! WARNING! WARNING!

CREATE INDEX test_index_table_test_uuid_idx 
ON test_index_table 
USING HASH (test_uuid);

-- Удаление индекса
DROP INDEX test_index_table_test_uuid_idx;

-- Выборка по uuid колонке
-- Без индекса - 36 sec
-- С индексом - 78 ms
SELECT *
FROM test_index_table
WHERE test_uuid = '1f99ac1f-3276-41b3-912a-5f202a80dbaf'
LIMIT 1000;

-- Проверить размер индекса
SELECT pg_size_pretty(
    pg_total_relation_size('test_index_table_test_uuid_idx'));
