-- Создание индекса на int колонку
-- занимает примерно 4 min 16sec ☕
CREATE INDEX test_index_table_test_number_idx 
ON test_index_table (test_number);

-- Удаление индекса
DROP INDEX test_index_table_test_number_idx;

-- Выборка по int колонке
-- Без индекса - 8.6 секунд
-- С индексом - 55 мс
SELECT *
FROM test_index_table
WHERE test_index_table.test_number = 10
LIMIT 1000;

-- Создание индекса на float колонку
-- занимает примерно 1 min 20sec ☕
CREATE INDEX test_index_table_test_float_idx 
ON test_index_table (test_float);

-- Удаление индекса
DROP INDEX test_index_table_test_float_idx;

-- Выборка по float колонке
-- Без индекса - 200мс
-- С индексом - 60мс
SELECT *
FROM test_index_table
WHERE test_index_table.test_float BETWEEN 4.0 AND 4.1
LIMIT 1000;
